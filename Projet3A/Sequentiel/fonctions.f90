module fonctions
use parametres
implicit none

real*8, parameter::pi=acos(-1.)

contains

real*8 function f (x,y,s)
	real*8::x,y,s
	if (cas==1) then
		f=2*(y-y**2+x-x**2)
	elseif (cas==2) then
		f=(sin(x)+cos(y))

	else
		f=exp(-(x-Lx/2)**2)*exp(-(y-Ly/2)**2)*cos(pi*s/2)
	end if
end function


real*8 function g (x,y)
	real*8::x,y
	if(cas==1) then
		g=0
	elseif (cas==2) then
		g=sin(x)+cos(y)
	else
		g=0
	end if
end function


real*8 function h (x,y)
	real*8::x,y
	if(cas==1) then
		h=0
	elseif (cas==2) then
		h=sin(x)+cos(y)
	else
		h=1
	end if
end function

subroutine gradconjtrival(A,x,b)
	implicit none
	real*8,dimension(3)::A
	real*8, dimension(:)::x,b
	real*8, dimension(:),allocatable::p,r,q,rp
	real*8::beta, alpha
	integer::n,i,k,compteur
	
	compteur = 0
	n = size(x)
	
	allocate(p(n),r(n),q(n),rp(n))
	r = b-matmultrival(A,x)
	p = r
	rp= r
	do while(dot_product(r,r)/dot_product(b,b)>eps*eps .and. compteur<itermax)
	
		compteur = compteur+1
		if (compteur>= itermax) then
			print*, "Gradient conjugué : Convergence non atteinte ! "
		endif

		q = matmultrival(A,p)
		alpha = dot_product(r,r)/dot_product(p,q)
		x = x + alpha*p
		rp=r
		r = r - alpha*q
		beta = dot_product(r,r)/dot_product(rp,rp)
		p = r + beta*p
	end do
	!print*,compteur,dot_product(r,r)/dot_product(b,b)
	deallocate(p,r,q,rp)
end subroutine



function matmultrival(A,x)
	implicit none
	real*8,dimension(3)::A
	real*8,dimension(:)::x
	real*8, dimension(:), allocatable::matmultrival
	integer::n,i

	n = size(x)
	allocate(matmultrival(n))
	
	!Premier bloc
	do i=1,Ny
		if (mod(i,Ny) == 0) then
			matmultrival(i) = A(3)*x(i+Ny)+A(2)*x(i-1)+A(1)*x(i)
		elseif (mod(i,Ny) == 1) then
			matmultrival(i) = A(3)*x(i+Ny)+A(2)*x(i+1)+A(1)*x(i)
		else
			matmultrival(i) = A(3)*x(i+Ny)+A(2)*(x(i-1)+x(i+1))+A(1)*x(i)
		end if
	end do
	!Dernier bloc
	do i=n-Ny+1,n
		if (mod(i,Ny) == 0) then
			matmultrival(i) = A(3)*x(i-Ny)+A(2)*x(i-1)+A(1)*x(i)
		elseif (mod(i,Ny) == 1) then
			matmultrival(i) = A(3)*x(i-Ny)+A(2)*x(i+1)+A(1)*x(i)
		else
			matmultrival(i) = A(3)*x(i-Ny)+A(2)*(x(i-1)+x(i+1))+A(1)*x(i)
		end if
	end do
	
	!Blocs centraux
	do i=Ny+1,n-Ny
		if (mod(i,Ny) == 0) then !dernière ligne du bloc
			matmultrival(i) = A(3)*(x(i-Ny)+x(i+Ny))+A(2)*x(i-1)+A(1)*x(i)
		elseif (mod(i,Ny) == 1) then !première ligne du bloc
			matmultrival(i) = A(3)*(x(i-Ny)+x(i+Ny))+A(2)*x(i+1)+A(1)*x(i)
		else ! lignes centrales du bloc
			matmultrival(i) = A(3)*(x(i-Ny)+x(i+Ny))+A(2)*(x(i-1)+x(i+1))+A(1)*x(i)
		end if
	end do
end function


  real*8 function prodscal (r,s)

    real*8, dimension(:),allocatable::r,s
    integer::i
    prodscal=0
    do i=1,size(r)
       prodscal=prodscal+(r(i)*s(i))
    end do
  end function prodscal



 real*8 function norme (r)
    real*8, dimension(:)::r
    integer::i
    norme=0
    do i=1,size(r)
       norme=norme+(r(i))**2
    end do
    norme=sqrt(norme)
  end function norme

subroutine rempAdiag (A)
    real*8,dimension(3)::A

     A=0
     A(1)=1+dt*2*D*Isdx2+dt*2*D*Isdy2
     A(3)=-D*dt*Isdx2
     A(2)=-D*dt*Isdy2
end subroutine

subroutine secondMembre(b,s)
	implicit none
	real*8,dimension(:),intent(out)::b
	integer::n,k,i,j
	real*8::x,y,s

	b=0
	do i=1,Nx
		x=(i-1)*dx
	
		do j=1,Ny
			y=j*dy
			!print*,x,y
			b((i-1)*Ny+j)=b((i-1)*Ny+j)+f(x,y,s)

			if (j==1 .OR. j==Ny) then
				b((i-1)*Ny+j)= b((i-1)*Ny+j) +D*dt*g(x,y)/(dy**2)
			end if
			
			if (i==1) then
				b((i-1)*Ny+j)= b((i-1)*Ny+j) +D*dt*h(0D0,j*dy)/(dx**2)
			end if
				
			if (i==Ny) then
				b((i-1)*Ny+j)= b((i-1)*Ny+j) +D*dt*h(Lx,j*dy)/(dx**2)
			end if
		end do
	end do
				
				
end subroutine


subroutine jacobi (A, U, B)
 
    !arguments
    real*8, dimension(:) :: U, B
    real*8, dimension(:) :: A
    integer :: nbCol

    !variables locales
    integer :: tailleU, k, compteur
    real*8, dimension(size(U)) :: Unew
    real*8 :: r

    !corps de la subroutine
  nbCol=1  
  compteur = 0
  
  tailleU = size(U)

  r = norme(matmultrival(A,U)-b)/max(norme(U),1.)
    do while(r>eps .and. compteur<itermax)
!print*,"test 2"
     compteur = compteur+1
     if (compteur>= itermax) then
        print*, "Jacobi : Convergence non atteinte! "
     endif


     do k = 1, tailleU

        !premier boucle if qui regarde si on est sur le premier, le dernier ou un bloc du milieu

        if ((k-1)/Ny==0) then        !cas du premier bloc
           if(k==1) then             !cas où on est à la première itération du premier bloc
              Unew(k) = 1./A(1)*(B(k)-(A(2)*U(k+1)+A(3)*U(k+Ny)))
           elseif (k == Ny) then     !cas où on est dans la dernière itération du premier bloc
              Unew(k) = 1./A(1)*(B(k)-(A(2)*U(k-1)+A(3)*U(k+Ny)))
           else                      !cas général du premier bloc
              Unew(k) = 1./A(1)*(B(k)-(A(2)*U(k-1)+A(2)*U(k+1)+A(3)*U(k+Ny)))
           endif                     !tous les cas du premier bloc ont été traités

        elseif ((k-1)/Ny==nbCol-1) then !cas du dernier bloc
           if(k == Ny*(nbCol-1)+1) then !cas où on est à la première itération du dernier bloc
              Unew(k) = 1./A(1)*(B(k)-(A(3)*U(k-Ny)+A(2)*U(k+1)))
           elseif (k== tailleU) then !cas où on est à la dernière itération du dernier bloc
              Unew(k) = 1./A(1)*(B(k)-(A(3)*U(k-Ny)+A(2)*U(k-1)))
           else                      !cas général du dernier bloc
              Unew(k) = 1./A(1)*(B(k)-(A(3)*U(k-Ny)+A(2)*U(k-1)+A(2)*U(k+1)))
           endif                     !tous les cas du dernier bloc ont été traités

        else                         !cas d'un bloc du milieu
           if(mod(k,Ny)==1) then     !cas où on est à la première itération d'un bloc du milieu
              Unew(k) = 1./A(1)*(B(k)-(A(3)*U(k-Ny)+A(2)*U(k+1)+A(3)*U(k+Ny)))
           elseif(mod(k,Ny)==0) then !cas où on est à la dernière itération d'un bloc du milieu
              Unew(k) = 1./A(1)*(B(k)-(A(3)*U(k-Ny)+A(2)*U(k-1)+A(3)*U(k+Ny)))
           else                      !cas général d'un bloc du milieu
              Unew(k) = 1./A(1)*(B(k)-(A(3)*U(k-Ny)+A(2)*U(k-1)+A(2)*U(k+1)+A(3)*U(k+Ny)))
           endif                     !tous les cas d'un bloc du milieu ont été traités

        endif                        !fermeture de la boucle sur les blocs, normalement absolument tous les cas ont été traités
     end do
     U=Unew                          !U récupère les valeurs calculées

	r = norme(matmultrival(A,Unew)-B)/norme(B)


    ! print*, compteur, r
  end do
end subroutine jacobi

subroutine gauss_seidel (A, U, B, nbCol)
    implicit none

    !arguments
    real*8, dimension(:) :: U, B
    real*8, dimension(:) :: A
    integer :: nbCol
    
    !variables locales
    integer :: tailleU, k, compteur
    real*8, dimension(size(U)) :: Unew
    real*8 :: r

    !corps de la subroutine
    compteur = 0
  
    tailleU = size(U)

    r = norme(matmultrival(A,U)-B)/max(norme(U),1.)

    do while(r>eps .and. compteur<itermax)
       compteur = compteur+1

       if (compteur>= itermax) then
          print*, "Gauss Seidel : Convergence non atteinte ! "
         
       endif

       do k = 1, tailleU

          !première boucle if qui regarde si on est sur le premier bloc, un bloc du milieu ou le dernier bloc

          if ((k-1)/Ny==0) then                         !cas du premier bloc
             if(k==1) then                              !cas où on est à la première itération du premier bloc
                Unew(k) = 1./A(1)*(B(k)-(A(2)*U(k+1)+A(3)*U(k+Ny)))
             else if (k /=1 .and. K/=Ny) then           !cas général du premier bloc
                Unew(k) = 1./A(1)*(B(k)-(A(2)*Unew(k-1)+A(2)*U(k+1)+A(3)*U(k+Ny)))
             else if (k == Ny) then                     !cas où on est dans la dernière itération du premier bloc
                Unew(k) = 1./A(1)*(B(k)-(A(2)*Unew(k-1)+A(3)*U(k+Ny)))      
             endif                                      !tous les cas du premier bloc ont été traités

          elseif((k-1)/Ny/=0 .and. (k-1)/Ny/=nbCol-1)then  !cas d'un bloc du milieu
             if(mod(k,Ny)==1) then                      !cas où on est à la première itération d'un bloc du milieu
                Unew(k) = 1./A(1)*(B(k)-(A(3)*Unew(k-Ny)+A(2)*U(k+1)+A(3)*U(k+Ny)))
             elseif(mod(k,Ny)/=1 .and. mod(k,Ny)/=0)then!cas général d'un bloc du milieu
                Unew(k) = 1./A(1)*(B(k)-(A(3)*Unew(k-Ny)+A(2)*Unew(k-1)+A(2)*U(k+1)+A(3)*U(k+Ny)))
             elseif(mod(k,Ny)==0) then !cas où on est à la dernière itération d'un bloc du milieu
                Unew(k) = 1./A(1)*(B(k)-(A(3)*Unew(k-Ny)+A(2)*Unew(k-1)+A(3)*U(k+Ny)))
             endif                                      !tous les cas d'un bloc du milieu ont été traités        

          
          elseif((k-1)/Ny==nbCol-1) then                   !cas du dernier bloc
             if(k == Ny*(nbCol-1)+1) then                  !cas où on est à la première itération du dernier bloc
                Unew(k) = 1./A(1)*(B(k)-(A(3)*Unew(k-Ny)+A(2)*U(k+1)))
             elseif(mod(k,Ny)/=1 .and. mod(k,Ny)/=0)then!cas général du dernier bloc
                Unew(k) = 1./A(1)*(B(k)-(A(3)*Unew(k-Ny)+A(2)*Unew(k-1)+A(2)*U(k+1)))
             elseif (k== tailleU) then                  !cas où on est à la dernière itération du dernier bloc
                Unew(k) = 1./A(1)*(B(k)-(A(3)*Unew(k-Ny)+A(2)*Unew(k-1)))
             endif                                      !tous les cas du dernier bloc ont été traités
          endif                        !fermeture de la boucle sur les blocs, normalement absolument tous les cas ont été traités
       end do
       U=Unew                          !U récupère les valeurs calculées
       r = norme(matmultrival(A,Unew)-B)/norme(B)
!       print*, compteur, r
    end do
  end subroutine gauss_seidel




 end module fonctions

