module gif
use parametres
contains

!***********************************************************
!               Function GIF_Frame
!***********************************************************
!Creates GIF data
subroutine GIF_Frame(Frame_Number,Frame_max,U)
	implicit none
	integer::i,j
	integer,intent(in)::Frame_Number
	integer,intent(inout)::Frame_max
	!Contains the values of the mesh points
	real*8,dimension(1:Nx)::X
	real*8,dimension(1:Ny)::Y
	real*8,dimension(:),intent(in)::U
	!Creation of a character using on integer:
	!len=1 for the numbers [0:9]
	character(len=1)::number1
	!len=2 for the numbers [10:99]
	character(len=2)::number2
	!len=3 for the numbers [100:999]
	character(len=3)::number3
	!len=4 for the numbers [1000:9999]
	character(len=4)::number4

	do i=1,Nx
		X(i)=i*dx
	end do
	
	do i=1,Ny
		Y(i)=i*dy
	end do
	
	if (Frame_max<=9) then
		write(number1,'(I1)')Frame_max
	elseif ( Frame_max>=10 .AND. Frame_max<=99) then
		write(number2,'(I2)')Frame_max
	elseif ( Frame_max>=100 .AND. Frame_max<=999) then
		write(number3,'(I3)')Frame_max
	elseif ( Frame_max>=1000 .AND. Frame_max<=9999) then
		write(number4,'(I4)')Frame_max
	end if
	
	if (Frame_max<=9) then
		open(unit=1, file = "./GIF_temporary/"//number1, form='formatted',status='unknown',action='write')
	elseif ( Frame_max>=10 .AND. Frame_max<=99) then
		open(unit=1, file = "./GIF_temporary/"//number2, form='formatted',status='unknown',action='write')
	elseif ( Frame_max>=100 .AND. Frame_max<=999) then
		open(unit=1, file = "./GIF_temporary/"//number3, form='formatted',status='unknown',action='write')
	elseif ( Frame_max>=1000 .AND. Frame_max<=9999) then
		open(unit=1, file = "./GIF_temporary/"//number4, form='formatted',status='unknown',action='write')
	end if
	
		write(1,*)0,X
		do j=1,Ny
			write(1,*)Y(j),U((j-1)*Nx+1:j*Nx)
		end do
	
	close(1)
	
	Frame_max=Frame_max+1

end subroutine

subroutine GIF_Animation(Frame_max,range1,range2,Delay)
	implicit none
	real*8::range1,range2
	integer::i
	integer,intent(in)::Frame_max,Delay
	
		!GNUPLOT command file
		open(unit=2, file = "./GIF_temporary/command_line.txt", form='formatted',status='unknown',action='write')
		write(2,*)'set xrange[0:',Ly,']'
		write(2,*)'set yrange[0:',Lx,']'
	
		write(2,*)'set zrange[',range1,':',range2,']'
	
		write(2,'(A31,I2)')'set terminal gif animate delay ',Delay
		write(2,*)'set output "animation.gif"'
	
		do i=0,Frame_max-1
			if (i<=9) then
				write(2,'(A7,I1,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=10 .AND. i<=99) then
			write(2,'(A7,I2,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=100 .AND. i<=999) then
				write(2,'(A7,I3,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=1000 .AND. i<=9999) then
				write(2,'(A7,I4,A23)')'splot "',i,'" matrix nonuniform w l'
			end if
		end do
	close(2)
	
	CALL System('cd ./GIF_temporary ; gnuplot command_line.txt')
	CALL System('cp ./GIF_temporary/animation.gif .') 
		!Erase the GIF Directory
!	CALL System('rm -r GIF_temporary')
end subroutine


end module
