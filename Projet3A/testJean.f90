program testJean
use fonctions
use gif
implicit none

integer::k,FrameMax
real*8,dimension(:),allocatable::U,A,B,Up

!Creates the GIF_temporary Directory
	CALL System('mkdir GIF_temporary')

!print*, "Methode de ouf"
allocate (B(Nx*Ny),U(Nx*Ny),A(3),Up(Nx*Ny))
!!!!La numérotation globale va de haut en bas puis de gauche à droite

call rempAdiag (A)

do k=1,Nx*Ny
	U(k) = 100*rand()
end do
Up = U
!B = matmultrival(A,U)

call secondMembre(B,Bg,Bd,taille,beg,s)

U(:)=10
call gradconjtrival(A,U,B)
FrameMax=0
call GIF_Frame(1,FrameMax,U)
call GIF_Frame(2,FrameMax,U)


if (maxval(U-Up)>eps) then
	print*,"resultat",U-Up
else
	print*, "Convergé"
end if

call GIF_Animation(2,0d0,100d0,20)


end program
