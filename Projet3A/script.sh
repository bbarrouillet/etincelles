#!/bin/bash
# Indique au système que l'argument qui suit est le programme utilisé pour exécuter ce fichier
# En règle générale, les "#" servent à mettre en commentaire le texte qui suit comme ici
echo Script pour ecrire les temps de calcul



for ((p=1 ; p<3 ; p++))
	do
	for ((m=1 ; m<4 ; m++))
		do
		for ((c=1 ; c<4 ; c++))
			do
			echo p $p m $m c $c
			echo 2
			mpirun -n 2 ./calcpara.exe $p $m $c 1

			for ((np=3 ; np<13 ; np++))
				do echo $np
					 mpirun -n $np ./calcpara.exe $p $m $c 0
			done
		done
	done
done

exit 0
