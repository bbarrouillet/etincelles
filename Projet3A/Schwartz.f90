module Schwartz
use fonctions
!use gif
implicit none
include "mpif.h"



contains


subroutine additif (Ume,A,Bme,Me,Np,beg,en,taille, t,U)
include "mpif.h"
integer::Me,Np,bord,beg,i,statinfo,taille,en
integer, dimension(Mpi_status_size)::status
real*8,dimension(:),allocatable::A,Ume,Bme,droite,gauche,U,cvgauche,cvdroite
real*8::t,time,erreurme,erreur
bord=taille-partage
time=0
allocate(droite(Ny),gauche(Ny),cvdroite(Ny),cvgauche(Ny))



do while (time<t)
	erreur=10
 do while (erreur>tolerance)
	if (Np/=1) then

		if (me==0) then
			call MPI_SEND(Ume((bord-1)*Ny+1:(bord)*Ny),Ny,MPI_DOUBLE_PRECISION,1,Np+me+1,MPI_COMM_WORLD,statinfo)
			!print*, "send de",me, "a",1,Ume((bord-1)*Ny+1)
			do i=1,Ny
				gauche(i)=h(0D0,i*dy)
			end do

			call MPI_RECV(droite,Ny,MPI_DOUBLE_PRECISION,1,1,MPI_COMM_WORLD,status,statinfo)
			!print*, "recv de",1, "a", me, droite(1)

		else if (me==Np-1 ) then 

			call MPI_SEND(Ume(partage*Ny+1:(partage+1)*Ny),Ny,MPI_DOUBLE_PRECISION,Np-2,Np-1,MPI_COMM_WORLD,statinfo)
			!print*, "send de",me, "a",Np-2,Ume(partage*Ny+1)
			call MPI_RECV(gauche,Ny,MPI_DOUBLE_PRECISION,Np-2,Np+Np-1,MPI_COMM_WORLD,status,statinfo)
			!print*, "recv de",NP-2, "a", me, gauche(1)
			do i=1,Ny
				droite(i)=h(Lx,i*dy)
			end do

		else 
	

			call MPI_SEND(Ume(partage*Ny+1:(partage+1)*Ny),Ny,MPI_DOUBLE_PRECISION,me-1,me,MPI_COMM_WORLD,statinfo)
			call MPI_SEND(Ume((bord-1)*Ny+1:(bord)*Ny),Ny,MPI_DOUBLE_PRECISION,me+1,Np+me+1,MPI_COMM_WORLD,statinfo)
			!print*, "send de",me, "a",me-1,Ume(partage*Ny+1)
			!print*, "send de",me,"a",me+1,Ume((bord-1)*Ny+1)
			call MPI_RECV(gauche,Ny,MPI_DOUBLE_PRECISION,me-1,Np+me,MPI_COMM_WORLD,status,statinfo)
			call MPI_RECV(droite,Ny,MPI_DOUBLE_PRECISION,me+1,me+1,MPI_COMM_WORLD,status,statinfo)
			!print*, "recv de",me-1, "a", me,gauche(1)
			!print*, "recv de",me+1, "a", me, droite(1)
			if (time==2*dt) then
			!print*,droite(20),gauche(20),me
			end if
		
		
		end if
	end if

	call MPI_BARRIER(MPI_COMM_WORLD,statinfo)

	call secondMembre (Bme,gauche,droite,taille,beg,time)



	
	if (methode==1) then

		call jacobi (A, Ume, Bme,taille)

	elseif (methode==2) then
		call gauss_seidel (A, Ume, Bme,taille)
	elseif (methode==3) then
		call gradconjtrival (A,ume,bme) 
	end if
!print*, "ok gradient"
	call getUfromUme (U,Ume,me,beg,en,Np,floor(time/dt))

	if (Np/=1) then

		if (me==0) then
			call MPI_SEND(Ume((taille-1)*Ny+1:(taille)*Ny),Ny,MPI_DOUBLE_PRECISION,1,Np+me+1,MPI_COMM_WORLD,statinfo)
		!	print*, "send de",me, "a",1,Ume((bord-1)*Ny+1)

			call MPI_RECV(cvdroite,Ny,MPI_DOUBLE_PRECISION,1,1,MPI_COMM_WORLD,status,statinfo)
		!	print*, "recv de",1, "a", me, droite(1)
			cvdroite = abs(cvdroite-Ume((taille-partage)*Ny+1:(taille-partage+1)*Ny))
			erreurme = sqrt(dot_product(cvdroite,cvdroite))
		!	print*, "erreurme", erreurme
	
		else if (me==Np-1 ) then 

			call MPI_SEND(Ume(1:Ny),Ny,MPI_DOUBLE_PRECISION,Np-2,Np-1,MPI_COMM_WORLD,statinfo)
		!	print*, "send de",me, "a",Np-2,Ume(partage*Ny+1)
			call MPI_RECV(cvgauche,Ny,MPI_DOUBLE_PRECISION,Np-2,Np+Np-1,MPI_COMM_WORLD,status,statinfo)
		!	print*, "recv de",NP-2, "a", me, gauche(1)
			cvgauche = abs(cvgauche-Ume((partage-1)*Ny+1:(partage)*Ny))
			erreurme = sqrt(dot_product(cvgauche,cvgauche))
		!	print*, "erreurme", erreurme

		else 
	

			call MPI_SEND(Ume(1:Ny),Ny,MPI_DOUBLE_PRECISION,me-1,me,MPI_COMM_WORLD,statinfo)
			call MPI_SEND(Ume((taille-1)*Ny+1:(taille)*Ny),Ny,MPI_DOUBLE_PRECISION,me+1,Np+me+1,MPI_COMM_WORLD,statinfo)
		!	print*, "send de",me, "a",me-1,Ume(partage*Ny+1)
		!	print*, "send de",me,"a",me+1,Ume((bord-1)*Ny+1)
			call MPI_RECV(cvgauche,Ny,MPI_DOUBLE_PRECISION,me-1,Np+me,MPI_COMM_WORLD,status,statinfo)
			call MPI_RECV(cvdroite,Ny,MPI_DOUBLE_PRECISION,me+1,me+1,MPI_COMM_WORLD,status,statinfo)
		!	print*, "recv de",me-1, "a", me,gauche(1)
		!	print*, "recv de",me+1, "a", me, droite(1)	
			cvdroite = abs(cvdroite-Ume((taille-partage)*Ny+1:(taille-partage+1)*Ny))
			cvgauche = abs(cvgauche-Ume((partage-1)*Ny+1:(partage)*Ny))
			erreurme = max(sqrt(dot_product(cvgauche,cvgauche)), sqrt(dot_product(cvdroite,cvdroite)))
		!	print*, "erreurme", erreurme
		end if
	end if
	
	call MPI_ALLREDUCE(erreurme,erreur,1,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,statinfo)
	!print*, erreur
	end do
	time=time+dt
	call getUfromUme (U,Ume,me,beg,en,Np,floor(time/dt))
!~ 	if (me==0)then
!~ 		print*, "Time", floor(time/dt)
!~ 		!print*,U(Nx*(Ny-1):Nx*Ny)
!~ 		call GIF_Frame(int((time-dt)/dt),FrameMax,U)
!~ 	end if	

end do

end subroutine




subroutine multiplicatif (Ume,A,Bme,Me,Np,beg,en,taille,t,U) 
integer::Me,Np,taille,beg,en,i,statinfo,taille1,taille2,bord1,bord2,l
real*8,dimension(:),allocatable::A,Ume,Bme,droite,gauche,Bme1,Bme2,Ume1,Ume2,U,cvgauche,cvdroite
real*8::t,time,erreur,erreurme
integer, dimension(Mpi_status_size)::status
if ( Nx<2*Np+partage*(2*Np-1)) then
print*,Nx,2*Np+partage*(2*Np-1)
print*,"Les zones de partage dans SCHWARTZ MULTIPLICATIF sont trop grandes pour le domaine!!! FULL STOP"
STOP
end if

taille1=partage+(taille-partage)/2
taille2=taille-taille1+partage
bord1=taille1-partage
bord2=taille2-partage
allocate (Ume1(taille1*Ny),Ume2(taille2*Ny),Bme1(taille1*Ny),Bme2(taille2*Ny))
allocate(cvdroite(Ny),cvgauche(Ny))
Ume1=Ume(1:taille1*Ny)
Ume2=Ume((taille-taille2)*Ny+1:taille*Ny)



time=0
allocate(droite(Ny),gauche(Ny))

do while (time<t)
	erreur =10 
	 do while (erreur>tolerance)

	!Résolution sur les sous domaines impaires
	if (Np/=1) then
		if (me==0) then
			call MPI_SEND(Ume2((bord2-1)*Ny+1:(bord2)*Ny),Ny,MPI_DOUBLE_PRECISION,1,Np+me+1,MPI_COMM_WORLD,statinfo)
			!call MPI_RECV(droite,Ny,MPI_DOUBLE_PRECISION,1,1,MPI_COMM_WORLD,status,statinfo)
			do i=1,Ny
				gauche(i)=h(0d0,i*dy)
			end do
			do i=1,Ny
				droite(i)=Ume2(partage*Ny+i)
			end do
		else if (me==Np-1) then 
			!call MPI_SEND(Ume(partage*Ny+1:(partage+1)*Ny),Ny,MPI_DOUBLE_PRECISION,Np-2,Np-1,MPI_COMM_WORLD,statinfo)
			call MPI_RECV(gauche,Ny,MPI_DOUBLE_PRECISION,Np-2,Np+Np-1,MPI_COMM_WORLD,status,statinfo)
	
			do i=1,Ny
				droite(i)=Ume2(partage*Ny+i)
			end do
		else
			!call MPI_SEND(Ume(partage*Ny+1:(partage+1)*Ny),Ny,MPI_DOUBLE_PRECISION,me-1,me,MPI_COMM_WORLD,statinfo)
			call MPI_SEND(Ume2((bord2-1)*Ny+1:(bord2)*Ny),Ny,MPI_DOUBLE_PRECISION,me+1,Np+me+1,MPI_COMM_WORLD,statinfo)
			call MPI_RECV(gauche,Ny,MPI_DOUBLE_PRECISION,me-1,Np+me,MPI_COMM_WORLD,status,statinfo)
			!call MPI_RECV(droite,Ny,MPI_DOUBLE_PRECISION,me+1,me+1,MPI_COMM_WORLD,status,statinfo)
	
			do i=1,Ny
				droite(i)=Ume2(partage*Ny+i)
			end do
		end if
	end if

	call secondMembre (Bme1,gauche,droite,taille1,beg,time)

	if (methode==1) then
		call jacobi (A, Ume1, Bme1,taille1)
	elseif (methode==2) then
		call gauss_seidel (A, Ume1, Bme1,taille1)
	elseif (methode==3) then
		call gradconjtrival (A,ume1,bme1) 
	end if

	!Résolution sur les sous domaines paires


	if (Np/=1) then
		if (me==0) then
		
			call MPI_RECV(droite,Ny,MPI_DOUBLE_PRECISION,1,1,MPI_COMM_WORLD,status,statinfo)
			do i=1,Ny
				gauche(i)=Ume1((taille1-partage-1)*Ny+i)
			end do
		
		else if (me==Np-1) then 
			call MPI_SEND(Ume1(partage*Ny+1:(partage+1)*Ny),Ny,MPI_DOUBLE_PRECISION,Np-2,Np-1,MPI_COMM_WORLD,statinfo)
		
			do i=1,Ny
				droite(i)=h(Lx,i*dy)
			end do
			do i=1,Ny
				gauche(i)=Ume1((taille1-partage-1)*Ny+i)
			end do
		else
			call MPI_SEND(Ume1(partage*Ny+1:(partage+1)*Ny),Ny,MPI_DOUBLE_PRECISION,me-1,me,MPI_COMM_WORLD,statinfo)
			call MPI_RECV(droite,Ny,MPI_DOUBLE_PRECISION,me+1,me+1,MPI_COMM_WORLD,status,statinfo)
	
			do i=1,Ny
				gauche(i)=Ume1((taille1-partage-1)*Ny+i)
			end do
		end if
	end if

	call secondMembre (Bme2,gauche,droite,taille2,beg+taille-taille2,time)

	if (methode==1) then
		call jacobi (A, Ume2, Bme2,taille2)
	elseif (methode==2) then
		call gauss_seidel (A, Ume2, Bme2,taille2)
	elseif (methode==3) then
		call gradconjtrival (A,ume2,bme2) 
	end if
!print*, Ume1(50),me

	Ume(1:taille1*Ny)=Ume1
	Ume(taille1*Ny+1:taille*Ny)=Ume2(partage*Ny+1:taille2*Ny)
	!print*, "Taille Ume", size(Ume), me
	
	if (Np/=1) then

		if (me==0) then
			call MPI_SEND(Ume((taille-1)*Ny+1:(taille)*Ny),Ny,MPI_DOUBLE_PRECISION,1,Np+me+1,MPI_COMM_WORLD,statinfo)
		!	print*, "send de",me, "a",1,Ume((bord-1)*Ny+1)

			call MPI_RECV(cvdroite,Ny,MPI_DOUBLE_PRECISION,1,1,MPI_COMM_WORLD,status,statinfo)
		!	print*, "recv de",1, "a", me, droite(1)
			cvdroite = abs(cvdroite-Ume((taille-partage)*Ny+1:(taille-partage+1)*Ny))
			erreurme = sqrt(dot_product(cvdroite,cvdroite))
		!	print*, "erreurme", erreurme
	
		else if (me==Np-1 ) then 

			call MPI_SEND(Ume(1:Ny),Ny,MPI_DOUBLE_PRECISION,Np-2,Np-1,MPI_COMM_WORLD,statinfo)
		!	print*, "send de",me, "a",Np-2,Ume(partage*Ny+1)
			call MPI_RECV(cvgauche,Ny,MPI_DOUBLE_PRECISION,Np-2,Np+Np-1,MPI_COMM_WORLD,status,statinfo)
		!	print*, "recv de",NP-2, "a", me, gauche(1)
			cvgauche = abs(cvgauche-Ume((partage-1)*Ny+1:(partage)*Ny))
			erreurme = sqrt(dot_product(cvgauche,cvgauche))
		!	print*, "erreurme", erreurme

		else 
	

			call MPI_SEND(Ume(1:Ny),Ny,MPI_DOUBLE_PRECISION,me-1,me,MPI_COMM_WORLD,statinfo)
			call MPI_SEND(Ume((taille-1)*Ny+1:(taille)*Ny),Ny,MPI_DOUBLE_PRECISION,me+1,Np+me+1,MPI_COMM_WORLD,statinfo)
		!	print*, "send de",me, "a",me-1,Ume(partage*Ny+1)
		!	print*, "send de",me,"a",me+1,Ume((bord-1)*Ny+1)
			call MPI_RECV(cvgauche,Ny,MPI_DOUBLE_PRECISION,me-1,Np+me,MPI_COMM_WORLD,status,statinfo)
			call MPI_RECV(cvdroite,Ny,MPI_DOUBLE_PRECISION,me+1,me+1,MPI_COMM_WORLD,status,statinfo)
		!	print*, "recv de",me-1, "a", me,gauche(1)
		!	print*, "recv de",me+1, "a", me, droite(1)	
			cvdroite = abs(cvdroite-Ume((taille-partage)*Ny+1:(taille-partage+1)*Ny))
			cvgauche = abs(cvgauche-Ume((partage-1)*Ny+1:(partage)*Ny))
			erreurme = max(sqrt(dot_product(cvgauche,cvgauche)), sqrt(dot_product(cvdroite,cvdroite)))
		!	print*, "erreurme", erreurme
		end if
	end if
	
	call MPI_ALLREDUCE(erreurme,erreur,1,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,statinfo)
	end do
	time=time+dt
	call getUfromUme (U,Ume,me,beg,en,Np,floor(time/dt))
!~ 	if (me==0)then
!~ 		print*, "Time", floor(time/dt)
!~ 		!print*,U(Nx*(Ny-1):Nx*Ny)
!~ 		call GIF_Frame(int((time-dt)/dt),FrameMax,U)
!~ 	end if	

	
end do


end subroutine

subroutine getUfromUme (U,Ume,me,beg,en,Np,tag)
real*8, dimension(:) :: U,Ume
integer::me,r,moy,beg,en,i,statinfo,Np, tailleUme
integer:: begi, eni,tag,l
integer, dimension(Mpi_status_size)::status

	if (me/=0) then
		call MPI_SEND(Ume,size(Ume),MPI_DOUBLE_PRECISION,0,me+Np*tag,MPI_COMM_WORLD,statinfo)
 		!print*, "send", me, size(Ume)
 		!print*,"send",size(uME),Ume(301),me,"tag :", me+Np*tag
! 		print*, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
	elseif (me == 0) then
		U(1:en*Ny)=Ume
		!print*, "cop",(beg-1)*Ny+1,en*Ny,me
		do i=1,Np-1
 			r=mod(Nx-partage,Np)
 			moy=(Nx-partage)/Np
!~ 			if (r==0)then
!~ 				tailleUme=(((i+1)*moy+partage)*Ny)-((i*moy)*Ny+1)
!~  				print*, "rec",tailleUme,i
!~  				print*,(i*moy),(i+1)*moy+partage
!~ 				call MPI_RECV(U((i*moy)*Ny+1:((i+1)*moy+partage)*Ny),tailleUme,MPI_DOUBLE_PRECISION,i,i,MPI_COMM_WORLD,status,statinfo)
!~ 				
!~ 
!~ 			else
!~ 				if (i<mod(Nx-partage,Np))then
!~ 					tailleUme =((i+1)*moy+partage+i+1)*Ny-(((i*moy)+i)*Ny)
!~  					print*, "rec",tailleUme,i
!~  					print*,(i*moy)+i,(i+1)*moy+partage+i+1
!~ 					call MPI_RECV(U(((i*moy)+i)*Ny+1:((i+1)*moy+partage+i+1)*Ny),tailleUme,MPI_DOUBLE_PRECISION,i,i,MPI_COMM_WORLD,status,statinfo)
!~ 					
!~ 				else
!~ 					tailleUme = ((i+1)*moy+partage+r)*Ny-((i*moy+r)*Ny)
!~  					print*, "rec",tailleUme,i
!~  					print*, (i*moy+r), (i+1)*moy+partage+r
!~ 					call MPI_RECV(U((i*moy+r)*Ny+1:((i+1)*moy+partage+r)*Ny),tailleUme,MPI_DOUBLE_PRECISION,i,i,MPI_COMM_WORLD,status,statinfo)
!~ 					
!~ 				end if
!~ 			end if
				if (i<r) then
					begi=i*moy+i+1
					eni=(i+1)*moy+partage+i+1
				else
					begi=i*moy+r+1
					eni=(i+1)*moy+partage+r
				end if
				tailleUme=(eni-begi+1)*Ny
 			!	print*, "rec",(begi-1)*Ny+1,eni*Ny,i
 			!	print*,begi,eni
			!	print*, i+(Np+1)*tag, i
				call MPI_RECV(U((begi-1)*Ny+1:eni*Ny),tailleUme,MPI_DOUBLE_PRECISION,i,i+Np*tag,MPI_COMM_WORLD,status,statinfo)
				
				!print*, "rec",i,(begi-1)*Ny+1,eni*Ny
!~ 				if (i+Np*tag>404) then
!~ 				DO l=1,480
!~ 				if (U((begi-1)*Ny+l)==0) then
!~ 				print*, "rec",tailleUme ,U((begi-1)*Ny+l), i,"tag :",i+Np*tag,(begi-1)*Ny+l
!~ 				end if
!~ 				end do
!~ 				end if
		end do
	end if
	call MPI_BARRIER(MPI_COMM_WORLD,statinfo)
!~ 	if (me==0)then
!~ 		print*,U(((Np-1)*moy)+r+1)
!~ 		print*,U(Nx*(Ny-1):Nx*Ny)
!~ 	end if	
end subroutine

end module










