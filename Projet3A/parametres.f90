module parametres
	implicit none
	!!!Le domaine est un rectangle discrétisé. Le maillage est cartésien et comporte Ny mailles par colonne et Nx mailles par lignes.
real*8::			dt,&
					D,&
					eps,&
					Lx,&
					Ly,&
					tolerance
!!!!La numérotation globale va de bas en haut puis de gauche à droite
!!!!Partage est le nombre de colones partagées par deux domaines cote à cote.

integer::			Nx,&
					Ny,&
					partage,&
					methode,&!!!Méthode: 1=Jacobi, 2=Gauss Seidel, 3=Gradient conjugué
					cas_parallelisme,&!!!Méthode de parallélisme: 1=Schwartz additif, 2=Schwartz multiplicatif
					cas,&
					itermax
real*8	::			t
real*8	::			dx,&
					dy,&
					Isdy2,&
					Isdx2		
integer::statinfo,framemax,Nt	


contains

subroutine InitParametres(me)
	implicit none
	integer::me
	
	open(unit=1+me,file="parametres.txt",status="old",form='formatted')
	
	read(1+me,*)
	read(1+me,*)
	read(1+me,*) dt
	read(1+me,*) D
	read(1+me,*) eps
	read(1+me,*) Lx
	read(1+me,*) Ly
	read(1+me,*) tolerance
	read(1+me,*) Nx
	read(1+me,*) Ny
	read(1+me,*) partage
	read(1+me,*) methode
	read(1+me,*) cas_parallelisme
	read(1+me,*) cas
	read(1+me,*) itermax
	
	dx=Lx/(Nx+1)
	dy=Ly/(Ny+1)
	Isdy2=1./(dy*dy)
	Isdx2=1./(dx*dx)	

end subroutine



end module
