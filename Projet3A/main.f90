program main
use fonctions
use parametres
use Schwartz
!use gif
implicit none


integer::Me,Np,beg,en,k,i,j

real*8,dimension(:),allocatable::U,A,B,Ume,Bme,t1,t2
real*8::td
character(len=1)::cas_parallelismec,methodec,casc,newc
integer::new
integer, dimension(Mpi_status_size)::status

FrameMax=0

call MPI_INit(statinfo)
  td=MPI_WTIME()
  call MPI_COMM_RANK(MPI_COMM_WORLD,Me,statinfo)
  call MPI_COMM_SIZE(MPI_COMM_WORLD,Np,statinfo)
 
	

 call InitParametres(me)
 
 allocate (B(Nx*Ny),U(Nx*Ny),A(3),t1(Np),t2(Np))
 
    CALL getarg(1, cas_parallelismec)
    CALL getarg(2, methodec)
    CALL getarg(3, casc)
    CALL getarg(4, newc)
    
    read(cas_parallelismec,*) cas_parallelisme
    read(methodec,*) methode
    read(casc,*) cas
    read(newc,*) new
 
 call CPU_time(t1(me+1))
 U=0
 if (cas==3) then
	t=4
	Nt=t/dt
else
	t=dt
	Nt=1
endif

if (partage*(Np-1)+Np>Nx) then
print*,"Les zones de partage sont trop grandes par rapport à la taille du domaine. FULL STOP"
STOP
end if
!!Le remplissage du second membre se fait dans Schwartz. PAS DANS MAIN
!!Verifier le premier argument
call colonnes (Np,Me,beg,en)
!!!!beg et en sont les indices de la première et la dernière colonne gérées par me
call rempAdiag (A)

!!!Déclaration des vecteurs B et U personnels au processeur me
allocate (Bme((en-beg+1)*Ny),Ume((en-beg+1)*Ny))



if (cas_parallelisme==1) then
	call additif(Ume,A,Bme,Me,Np,beg,en,en-beg+1,t,U)
else
	call multiplicatif(Ume,A,Bme,Me,Np,beg,en,en-beg+1,t,U)
end if

	call CPU_time(t2(me+1))
	
	
	if (me > 0) then
		call MPI_SEND(t2(me+1),1,MPI_DOUBLE_PRECISION,0,me,MPI_COMM_WORLD,statinfo)
	else	
		do i = 1,Np-1
			call MPI_RECV(t2(i+1),1,MPI_DOUBLE_PRECISION,i,i,MPI_COMM_WORLD,status,statinfo)
		end do
		
		open(unit=1,file="results.csv",status="old",form='formatted',position='append')
		
		if (new == 1) then
		  write(1,*) 'tolerance solveur : ',';', eps,';',&
		  & 'tolerance superposition : ',';', tolerance,';',&
		  & 'Nx : ',';', Nx,';',&
		  & 'Ny : ',';', Ny,';',&
		  & 'taille de partage : ',';', partage
		  
		  write(1,*)
		  
		  select case(cas_parallelisme)		  
		    case (1)
		      write(1,*) 'Schwartz additif'
		    case (2)
		      write(1,*) 'Schwartz multiplicatif'
		  end select
		  
		  select case(methode)		  
		    case (1)
		      write(1,*) 'Jacobi'
		    case (2)
		      write(1,*) 'Gauss Seidel'
		    case (3)
		      write(1,*) 'Gradient conjugue'
		  end select
		  
		  select case(cas)		  
		    case (1)
		      write(1,*) 'cas 1'
		    case (2)
		      write(1,*) 'cas 2'
		    case (3)
		      write(1,*) 'cas 3'
		  end select
		  write(1,*) 'Np',';', 'temps'	
		end if
		
		
		
		write(1,*) NP,';', maxval(t2-t1)	
		
		
		close(1)
		
	endif
	
!~ if (me==0) then
!~ 	if (cas==1) then
!~ !CALL System('mkdir GIF_temporary')
!~ 		call GIF_Animation(floor(t/dt),0d0,5d-1,5)
!~ 	else if(cas==2) then
!~ 		call GIF_Animation(floor(t/dt),0d0,2d0,5)
!~ 	else
!~ 		call GIF_Animation(floor(t/dt),-2d-1,1d0,5)
!~ 	end if
!~ end if



call MPI_finalize(statinfo)




end program



























