program testJeanGui
use fonctions
implicit none

integer::entierK
real*8,dimension(:),allocatable::U,A,B,Up,Ubis


!!!Le domaine est un rectangle discrétisé. Le maillage est cartésien et comporte Ny mailles par colonne et Nx mailles par lignes.

!!!méthode: 1=Jacobi, 2=Gauss Seidel, 3=Gradient conjugué
!methode=1
!print*, "Methode de ouf"
allocate (B(Nx*Ny),U(Nx*Ny),A(3),Up(Nx*Ny), Ubis(Nx*Ny))
!!!!La numérotation globale va de haut en bas puis de gauche à droite


A(1)=4
A(2)=-1
A(3)=-1

do entierK=1,Nx*Ny
	U(entierK) = 100*rand()
end do
Up = U
B = matmultrival(A,U)
U(:)=10
Ubis(:)=10
call jacobi(A, U, B, Nx)
call gauss_seidel(A, Ubis, B, Nx)
do entierK=1,size(U)
	print*,"test 18"
	print*,"Up, U, Ubis",Up(entierK),U(entierK), Ubis(entierK)
	
end	do
do entierK=1, size(U)
print*, U(entierK)-Up(entierK), Ubis(entierK)-Up(entierK)
end do
end program testJeanGui
