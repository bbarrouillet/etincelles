module gradconj
  use MPI
  implicit none


contains


  subroutine notloc (i,j,notglo,Nx)
    integer,intent(in)::notglo,Nx
    integer,intent(out)::i,j

    i=1+ mod(notglo-1,Nx)   
    j=1+(notglo-1)/Nx

  end subroutine notloc








  subroutine rempA (A,dx,dy,dt,Nx,Ny,D)
    integer::Nx,Ny,i,n
    real::dx,dy,dt,D
    real,dimension(:,:)::A
    A=0
    n=Nx*Ny
    print*,dt,dx,dy
    do i=1,n
       A(i,i)=1+dt*2*D/(dx**2)+dt*2*D/(dy**2)

    end do
    print*,dt*2*D/(dx**2),dt*2*D/(dy**2),A(1,1)
    do i=1,n-1
       if (mod(i,Nx)/=0)then
          A(i+1,i)=-D*dt/(dx**2)
          A(i,i+1)=-D*dt/(dx**2)
       end if
    end do

    do i=1,n-Nx
       A(i,i+Nx)=-D*dt/(dy**2)
       A(i+Nx,i)=-D*dt/(dy**2)

    end do
  end subroutine rempA


  subroutine rempAdiag (A,dx,dy,dt,D,Isdy2,Isdx2)
    real::dx,dy,dt,D,Isdy2,Isdx2
    real,dimension(3)::A
    A=0


    A(1)=1+dt*2*D*Isdx2+dt*2*D*Isdy2
    A(2)=-D*dt*Isdx2
    A(3)=-D*dt*Isdy2

!!$do i=1,n-1
!!$       if (mod(i,Nx)/=0)then
!!$          A(2,i)=-D*dt/(dx**2)
!!$          A(4,i+1)=-D*dt/(dx**2)
!!$       end if
!!$    end do
!!$
!!$    do i=1,n-Nx
!!$       A(1,i)=-D*dt/(dy**2)
!!$       A(5,i+Nx)=-D*dt/(dy**2)
!!$    end do
  end subroutine rempAdiag



  function valdiag (i,j,Adiag,Nx,Ny)
    integer::i,j,Nx,Ny
    real,dimension(3)::Adiag
    real::valdiag
 
    if (i==j-1 .AND. mod(i,Nx)/=0) then
       valdiag=Adiag(2)
    elseif (i==j+1 .AND. mod(j,Nx)/=0) then
       valdiag=Adiag(2)
    elseif (i==j-Nx) then
       valdiag=Adiag(3)
    elseif (i==j+Nx) then
       valdiag=Adiag(3)
    elseif (i/=j)then 
       valdiag=0
    elseif (i==j) then
       valdiag=Adiag(1)
    end if
  end function valdiag



  real function norme (r,beg,en)
    real, dimension(:),allocatable::r
    integer::i,beg,en
    norme=0
    do i=beg,en
       norme=norme+(r(i))**2
    end do
    norme=sqrt(norme)
  end function norme






  real function prodscal (r,s,beg,en)
    real, dimension(:),allocatable::r,s
    integer::i,beg,en
    prodscal=0
    do i=beg,en
       prodscal=prodscal+(r(i)*s(i))
    end do
  end function prodscal





  subroutine charge (n,Np,me,i1,en)
    implicit none
    integer::n,Np,me,i1,en,r
    r=mod(n,Np)

    if (me<Np-r) then
       i1=me*(n/Real(Np))+1
       en=(me+1)*(n/Real(Np))

    else     
       i1=(Np-r)*(n/Real(Np))+(me-Np+r)*(n/Real(Np))+1
       en=(Np-r)*(n/Real(Np))+(me-Np+r+1)*(n/Real(Np))

    end if

  end subroutine charge


  subroutine matvectpar (A,b,n,me,Np)
    integer::n,me,Np
    real,dimension(:,:)::A
    real,dimension(:)::b

  end subroutine matvectpar


  !A*k=b le proc me connait partiellement b et k ATTENTION LE PROC DOIT DEJA CONNAITRE K ETTENDU
  subroutine gradconjugue (Adiag,b,k,eps,nl,n,me,Np,beg,en,statinfo,Nx,Ny)
    implicit none
    real,dimension(3)::Adiag
    real, dimension(:),allocatable::b,d,r,w,k,s
    real, dimension(2)::prodrw,P1
    integer::n,nl,i,j,l,me,Np,beg,en,statinfo,Nx,Ny
    real::alpha, beta,eps,msg,norm_r,norm_s
    integer,dimension(MPI_status_size)::status
    real*8::tcom,tcal

   ! tcal=MPI_WTIME()

    allocate(r(beg:en),w(beg:en),s(beg:en))

    if (me==0)then
       allocate (d(beg:en+Nx))
    elseif (me==Np-1) then
       allocate (d(beg-Nx:en))
    else
       allocate (d(beg-Nx:en+Nx))
    end if


!!$    n=size(k)
    l=0

    r=0
    !k est connu sous sa forme éttendue 
 do i=beg,en
          r(i)=r(i)+valdiag(i,i,Adiag,Nx,Ny)*k(i)-b(i)
          if (i>Nx) then
          r(i)=r(i)+valdiag(i,i-Nx,Adiag,Nx,Ny)*k(i-Nx)
          end if
          if (i<=n-Nx) then
          r(i)=r(i)+valdiag(i,i+Nx,Adiag,Nx,Ny)*k(i+Nx)
          end if
           if (i>1) then
          r(i)=r(i)+valdiag(i,i-1,Adiag,Nx,Ny)*k(i-1)
          end if
          if (i<n) then
          r(i)=r(i)+valdiag(i,i+1,Adiag,Nx,Ny)*k(i+1)
          end if

       end do
  

    d(beg:en)=r
    !d est ici connu partiellement
    !Calcul de la norme de r: mpi all reduce pour norme (on choisit de faire la norme 2 |x|= sqrt(somme( |xi|**2))



    norm_r=1000


 !   print*,'cal1', MPI_WTIME()-tcal

    do while (l<=nl .AND. eps<norm_r)

       !On éttend d à l'aide des valeurs calculées par me+1 et me-1
       if (Np/=1) then
  !        tcom=MPI_WTIME()
          if (me==0) then
             call MPI_SEND(d(en-Nx+1:en),Nx,MPI_REAL,1,Np+me+1,MPI_COMM_WORLD,statinfo)
             call MPI_RECV(d(en+1:en+Nx),Nx,MPI_REAL,1,1,MPI_COMM_WORLD,status,statinfo)
          else if (me==Np-1) then 
             call MPI_SEND(d(beg:beg+Nx-1),Nx,MPI_REAL,Np-2,Np-1,MPI_COMM_WORLD,statinfo)
             call MPI_RECV(d(beg-Nx:beg-1),Nx,MPI_REAL,Np-2,Np-1+Np,MPI_COMM_WORLD,status,statinfo)
          else
             call MPI_SEND(d(beg:beg+Nx-1),Nx,MPI_REAL,me-1,me,MPI_COMM_WORLD,statinfo)
             call MPI_SEND(d(en-Nx+1:en),Nx,MPI_REAL,me+1,Np+me+1,MPI_COMM_WORLD,statinfo)
             call MPI_RECV(d(beg-Nx:beg-1),Nx,MPI_REAL,me-1,Np+me,MPI_COMM_WORLD,status,statinfo)
             call MPI_RECV(d(en+1:en+Nx),Nx,MPI_REAL,me+1,me+1,MPI_COMM_WORLD,status,statinfo)
          end if
       end if

       !test de temps
     !  if(l==1) then
     !  print*, 'com1', MPI_WTIME()-tcom
     !  end if

       w=0


 !      tcal=MPI_WTIME()
       

         


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! if (i>Nx .AND.i<=n-Nx) then
!w(i)=w(i)+Adiag(3)*d(i-Nx)+Adiag(3)*d(i+Nx)+valdiag(i,i-1,Adiag,Nx,Ny)*d(i-1)+valdiag(i,i+1,Adiag!,Nx,Ny)*d(i+1)

          !elseif(i>1 .AND.i<=Nx

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
do i=beg,en
          w(i)=w(i)+valdiag(i,i,Adiag,Nx,Ny)*d(i)
          if (i>Nx) then
          w(i)=w(i)+valdiag(i,i-Nx,Adiag,Nx,Ny)*d(i-Nx)
          end if
          if (i<=n-Nx) then
          w(i)=w(i)+valdiag(i,i+Nx,Adiag,Nx,Ny)*d(i+Nx)
          end if
           if (i>1) then
          w(i)=w(i)+valdiag(i,i-1,Adiag,Nx,Ny)*d(i-1)
          end if
          if (i<n) then
          w(i)=w(i)+valdiag(i,i+1,Adiag,Nx,Ny)*d(i+1)
          end if
     end do




!test de temps
     !  if(l==1) then
     !  print*, 'cal2', MPI_WTIME()-tcal
     !  end if

!tcom=MPI_WTIME()
       !w est connu partiellement (non éttendu)
prodrw(1)=prodscal(d,r,beg,en)
prodrw(2)=prodscal(d,w,beg,en)
       ! On fait le prod scalaire avec mpi all reduce


!produi scalaire envoyé à proc0 qui calcule et renvoie
!if (me/=0) then
!call MPI_SEND(prodw,1,MPI_REAL,0,2*Np+me+1,MPI_COMM_WORLD,statinfo)
! call MPI_SEND(prodr,1,MPI_REAL,0,Np+me+1,MPI_COMM_WORLD,statinfo)

!produit scalaire all reduce couteux en temps (10-4 s)
       call MPI_ALLREDUCE(prodrw,P1,2,MPI_REAL,MPI_SUM,MPI_COMM_WORLD,statinfo)
       











!test de temps
    !   if(l==1) then
    !   print*, 'com2', MPI_WTIME()-tcom
    !   end if

!tcal=MPI_WTIME()


       alpha=P1(1)/P1(2)

       !d et k sont déja connus de manière éttendue et on veut k éttendu pour la suite donc c'est bon ATTENTION, LORSQU'ON SORTIRA 
       k=k-alpha*d
       s=r
       r=r-alpha*w

       !On calcule la nouvelle norme de r et on conserve l'ancienne dans norm_s
       norm_s=norm_r
       msg=(norme (r,beg,en))**2
       norm_r=0


!test de temps
 !      if(l==1) then
 !      print*, 'cal3', MPI_WTIME()-tcal
 !      end if

!tcom=MPI_WTIME()

       call MPI_ALLREDUCE(msg,norm_r,1,MPI_REAL,MPI_SUM,MPI_COMM_WORLD,statinfo)

!test de temps
 !      if(l==1) then
 !      print*, 'com3', MPI_WTIME()-tcom
 !      end if

!tcal=MPI_WTIME()

       norm_r=sqrt(norm_r)


       beta=(norm_r/norm_s)**2
       d(beg:en)=r+beta*d(beg:en)

!test de temps
   !    if(l==1) then
   !    print*, 'cal4', MPI_WTIME()-tcal, '\n'
   !    end if


       l=l+1
    end do



  end subroutine gradconjugue


  subroutine Rename(Me,names)
    implicit none
    integer :: Me
    character*13 ::names
    character*3 :: tn
    integer :: i1,i2,i3
    i1 = Me/100
    i2 =( Me - 100*i1)/10
    i3 = Me - 100*i1 -10*i2
    tn = char(i1+48)//char(i2+48)//char(i3+48)
    names='sol'//tn//'.dat'
  end subroutine Rename

end module gradconj
