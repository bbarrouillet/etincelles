program test_gradient_conjugue
  use gradientconj
  use fonctions
  implicit none
  real*8 :: f,g,h,eps
  integer :: i,j,cas,Np !cas :choix du second membre, Np : nbr iter SOR
  real*8, dimension(:,:),pointer :: A
  real*8, dimension(:),pointer::B,U,Uo !B : second membre
 ! INITIALISATION DES PARAMETRES
  
  open(unit=1, file='parametres.txt')
  read (1,*) Nx
  read (1,*) Ny
  read (1,*) Lx
  read (1,*) Ly
  read (1,*) D
  read (1,*) dt
  close (1)

  allocate (U(Nx*Ny), Uo(Nx*Ny))
  allocate (B(Nx*Ny))

  dx=Lx/real(Nx+1)
  dy=Ly/real(Ny+1)
  eps=1E-6
  Np=250
  allocate (A(Nx*Ny,Nx*Ny))
  A=0.d0
  do i=1,Nx*Ny
     A(i,i)=1.d0
  end do
  Uo=1.d0
  B=4.d0
  print*,'Uo',Uo
  call grad_conj(Nx*Ny,eps,Np,B,Uo,A)
  print*,'erreur'
print*,'AUo-B',matmul(A,Uo)-B
end program
