module resolution
  implicit none
contains


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!GRADIENT CONJUGUE !!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  subroutine grad_conj(N,eps,Nit,F,U,A)
    implicit none
    real*8, dimension(:,:), pointer :: A
    real*8, dimension(:), pointer :: U,F !U : solution
    real*8, intent(in) :: eps
    integer, intent(in) :: N  !N taille de U 
    real*8, dimension(:), pointer :: d,r,w,r1
    real*8 :: alpha, rho
    integer :: l,Nit    ! Np nombre d iterations

    !allocation
    allocate (d(N),r(N),w(N),r1(N))
    !initialisation
    l=0
    r=matmul(A,U)-F
    d=r

    !boucle
    do while ( 0<=l .and. l<=Nit .and. sqrt(dot_product(r,r))>eps)
       !print*,'ecart :',sqrt(dot_product(r,r)),', nombre iter :',l
       w=(matmul(A,d))
       alpha=(dot_product(d,r))/(dot_product(d,w))
       U=U-alpha*d
       r1=r-alpha*w
       rho=(dot_product(r1,r1))/(dot_product(r,r))
       d=r1+rho*d        
       l=l+1
       r=r1  
    end do
  end subroutine grad_conj

end module resolution
