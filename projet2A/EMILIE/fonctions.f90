module initialisation
  implicit none
  real*8 :: dx, dy, dt, Lx, Ly, D,t
  integer :: Nx, Ny

contains 
  ! FONCTIONS f,g,h : second membre et conditions aux bords
  subroutine fonctionf(x,y,t,f,cas)
    implicit none
    real*8, intent(in) :: x,y,t
    real*8, intent(out) :: f
    integer :: cas
    if (cas ==1) then
       f = 2.d0*(y-y**2+x-x**2)
    elseif (cas==2) then
       f = sin(x) + cos(y)
    elseif (cas==3) then
       f = exp(-(x-Lx/2.d0)**2 -(y-Ly/2.d0)**2) * cos(3.14/2.d0*t)
    end if
  end subroutine fonctionf
 
 subroutine fonctiong(x,y,t,g,cas) 
    implicit none
    real*8, intent(in) :: x,y,t
    real*8, intent(out) :: g
    integer :: cas
    if (cas ==1) then
       g = 0.d0
    elseif (cas==2) then
       g = sin(x) + cos(y)
    elseif (cas==3) then
       g = 0.d0
    end if
  end subroutine fonctiong


 subroutine fonctionh(x,y,t,h,cas) 
    implicit none
    real*8, intent(in) :: x,y,t
    real*8, intent(out) :: h
    integer :: cas
    if (cas ==1) then
       h =0.d0
    elseif (cas==2) then
       h = sin(x) + cos(y)
    elseif (cas==3) then
       h = 1.d0
    end if
  end subroutine fonctionh


  !calcul de la matrice A
  ! <!> A(i,j) correspond a la ieme COLONNE et jieme LIGNE 'EN PARTANT DU BAS' de la matrice
subroutine matA(Nx,Ny,dx,dy,dt,D,A)
    implicit none
    real*8, dimension(:,:),pointer,intent(out) :: A
    real*8,intent(in) :: dx, dy, dt, D
    integer,intent(in) :: Nx,Ny
    integer ::i,j,k,i1,in
    real*8::K1,K2,K3
    allocate (A(Nx*Ny,Nx*Ny))
 
    A=0.d0

    !Stockage des 4 coeff qui interviennent dans la matrice
    K1=1.d0 + 2.d0*D*dt/(dx**2)+ 2.d0*D*dt/(dy**2) !diagonale
    K2=-D*dt/(dx**2) !juste sur et sous  diagonale
    K3=-D*dt/(dy**2) !Nx au dessus et dessous diagonale
       
    do j=1,Ny !Ny : nombre de 'bloc de lignes'      
       do i =(j-1)*Nx +1,(j-1)*Nx+Nx !Ny : taille d'un bloc
          A(i,i)= K1
          if (i/=(j-1)*Nx+1) then
             A(i-1,i) = K2
             A(i,i-1) = K2
          end if
          if (j/=1)then 
             A(i,i-Nx) = K3
             A(i-Nx,i) = K3
          end if
         ! if (j/=Ny) then
         !    A(i,i+Ny) = K3
         ! end if
       end do
    end do
  end subroutine matA

end module initialisation
