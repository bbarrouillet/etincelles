program chaleur

  use initialisation
  use resolution

  implicit none
  real*8 :: f,g,h,eps,kt,Tfinal,coeff1,coeff2
  integer :: i,j,cas,Np !cas :choix du second membre, Np : nbr iter SOR
  real*8, dimension(:,:),pointer :: A,sol
  real*8, dimension(:),pointer::B,U,Uo !B : second membre
  integer :: statinfo, me, He, k,Nit
  integer,dimension(:),pointer :: i1,in,j1,jn

  ! INITIALISATION DES PARAMETRES

  open(unit=1, file='parametres.txt')
  read (1,*) Nx
  read (1,*) Ny
  read (1,*) Lx
  read (1,*) Ly
  read (1,*) D
  read (1,*) dt
  close (1)

  allocate (U(Nx*Ny), Uo(Nx*Ny))
  allocate (B(Nx*Ny))
  allocate (sol(0:Nx+1,0:Ny+1))

  dx=Lx/real(Nx+1)
  dy=Ly/real(Ny+1)
  eps=1E-6
  Np=1E3

  ! CALCUL DES MATRICES ET VECTEURS
  
  !calcul de la matrice A
  call matA(Nx,Ny,dx,dy,dt,D,A)
  !test de A
!!$print*,'A'
!!$  do i=1,Nx*Ny
!!$     do j=1,Nx*Ny
!!$        if (A(i,j)/=0) then
!!$           print*,i,j,A(i,j)
!!$        end if
!!$     end do
!!$  end do

  Uo=1.d0
  U=2.d0
  Tfinal=100.d0
  
  !calcul du vecteur second membre B 
  cas =1 !choix des fonctions f,g,h
  coeff1=dt*D/(1.d0*dx**2)
  coeff2=dt*D/(1.d0*dy**2)
  do while (kt<=Tfinal .and. sqrt(dot_product((U-Uo),(U-Uo)))>eps)
     Uo=U
     do i=1,Nx
        do j=1,Ny
           call fonctionf(i*dx,j*dy,kt,f,cas)
           B((j-1)*Nx+i)=Uo((j-1)*Nx+i)+f*dt 
           !Calcul pour les termes de bords
           ! Gamma0
           if (i==1) then
              call fonctionh(0.d0,j*dy,kt,h,cas) !0=(i-1)*dx
              B((j-1)*Nx+i)=B((j-1)*Nx+i)+h*coeff1
           else if (i==Nx) then
              call fonctionh((Nx+1)*dx,j*dy,kt,h,cas) !Lx=(Nx+1)*dx
              B((j-1)*Nx+i)=B((j-1)*Nx+i)+h*coeff1  
           end if
           ! Gamma1
           if (j==1) then
              call fonctiong(i*dx,0.d0,kt,g,cas)
              B((j-1)*Nx+i)=B((j-1)*Nx+i)+g*coeff2
           else if (j==Ny) then
              call fonctiong(i*dx,(Ny+1)*dy,kt,g,cas)
              B((j-1)*Nx+i)=B((j-1)*Nx+i)+g*coeff2
           end if
        end do
     end do
     ! print*,'B',B
     call grad_conj(Nx*Ny,eps,Nit,B,U,A)
     kt=kt+dt
  end do
  print*,'temps final', kt-dt


  !ecriture de la solution U sous forme de matrice
  do j=1,Ny
     do i=1,Nx
        sol(i,j) = U(i+Nx*(j-1))
     end do
  end do

  do j=0,Ny+1
   call fonctionh(0.d0,j*dy,kt,h,cas)
   sol(0,j)=h
   call fonctionh((Nx+1)*dx,j*dy,kt,h,cas)
   sol(Nx+1,j)=h
end do

do i=0, Nx+1
   call fonctiong(i*dx,0.d0,kt,g,cas)
   sol(i,0)=g
   call fonctiong(i*dx,(Ny+1)*dy,kt,g,cas)
   sol(i,Ny+1)=g
end do


!ecriture de la solution dans un fichier pour la tracer
open(unit=1, file='solution.txt')
do i=0,Nx+1
   do j=0,Ny+1
      write(1,*),i*dx, j*dy, sol(i,j)
   end do
end do
close(1)

! Commandes
! splot 'solution.txt' u 1:2:3,sin(x)+cos(y)
! splot 'solution.txt' u 1:2:3,x*(x-1)*y*(y-1)

end program
