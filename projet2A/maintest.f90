program main
  use fonctions
  use gradconj
  use MPI

  implicit none

  real*8::td,te,tcom,tcal
  real::x,y,t,dx,dy,dt,D,Lx,Ly,eps,Isdx2,Isdy2
  integer::Nx,Ny,Nyloc,Np,statinfo,me,cas,beg,en
  integer::i,j,k,l,n,nl
  integer,dimension(MPI_status_size)::status
  real,dimension(3)::A
  real,dimension(:), allocatable::Un,Up,b,testb
  character*13 ::names

  open(unit=1000, file='parameter.dat', status='old', action='read')
  read(1000,*)Nx,Nyloc,dt,D,Lx,Ly
  close(1000)
  !  print*,Nx,Nyloc,dt,D,Lx,Ly
  dx=Lx/(Nx+1)
  Isdx2=1/dx/dx
  eps=0.000001
  cas=2
  nl=1000



!!!!!!!!!!!!!!!!!!!TZEST
  Ny=100
  dy=Ly/(Ny+1)
  !allocate (testb(Nx*Ny))




!!!!!!!!!!!!!!!!!!!!!!TESTS

  !   open(unit=2000,file="TEST_COND.dat") 

  !call notloc (k,l,9900,Nx)
  !print*,k,l
  !call notloc (k,l,800,Nx)

  !print*,k,l
  !print*,g(2,0.,0.),h(2,0.,0.),f(cas,0.990099013,0.980198026,0.,Lx,Ly),sin(0.990099013),cos(0.980198026)
  !	do i=1,Nx*Ny
  !        call notloc (k,l,i,Nx)!

  ! testb(i)=dt*f(cas,k*dx,l*dy,t,Lx,Ly)
  !
  !if (k==1) then
  !testb(i)=testb(i)+h(cas,k*dx,l*dy)*dt*D*Isdx2
  !elseif (k==Nx) then
  !testb(i)=testb(i)+h(cas,k*dx,l*dy)*dt*D*Isdx2
  !e!nd if!

  !if (l==1) then
  !testb(i)=testb(i)+g(cas,k*dx,l*dy)*dt*D*Isdy2
  !elseif (l==Ny) then
  !testb(i)=testb(i)+g(cas,k*dx,l*dy)*dt*D*Isdy2
  !end if  

  !write(2000,*) k*dx,l*dy,testb(i)

  !end do


  !close(2000)


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



 call rempAdiag (A,dx,dy,dt,D,Isdy2,Isdx2)




  call MPI_INit(statinfo)
  td=MPI_WTIME()
  call MPI_COMM_RANK(MPI_COMM_WORLD,Me,statinfo)
  call MPI_COMM_SIZE(MPI_COMM_WORLD,Np,statinfo)
  t=0

  Ny=Nyloc*Np
  dy=Ly/(Ny+1)
  Isdy2=1/dy/dy
  call charge (Nx*Ny,Np,Me,beg,en)




  allocate (Un(beg:en),b(beg:en))

  if (me==0)then
     allocate (Up(beg:en+Nx))
  elseif (me==Np-1) then
     allocate (Up(beg-Nx:en))
  else
     allocate (Up(beg-Nx:en+Nx))
  end if


  n=Ny*Nx
  !print*,n
  Up=0
  Un=0

  !Up est déjà éttendu car nul, rien ne sert de le rééttendre! attention si on change la valeur de Up initiale






  do while (t<1)



     do i=beg,en
        call notloc (k,l,i,Nx)

        b(i)=Un(i)+dt*f(cas,k*dx,l*dy,t,Lx,Ly)

        if (k==1) then
           b(i)=b(i)+h(cas,0.,l*dy)*dt*D*Isdx2
        elseif (k==Nx) then
           b(i)=b(i)+h(cas,1.,l*dy)*dt*D*Isdx2
        end if

        if (l==1) then
           b(i)=b(i)+g(cas,k*dx,0.)*dt*D*Isdy2
        elseif (l==Ny) then
           b(i)=b(i)+g(cas,k*dx,1.)*dt*D*Isdy2
        end if

     end do







     call gradconjugue(A,b,Up,eps,nl,n,me,Np,beg,en,statinfo,Nx,Ny)


     !Up est perpétuellement étendu, on stock la valeur non éttendue de la solution dans Un.
     Un(beg:en)=Up(beg:en)



     t=t+dt

     !if (me==0) then
     !  print*, t
     !end if
     !print*,t




  end do



  te=MPI_WTIME()-td
  call MPI_ALLREDUCE(te,te,1,MPI_REAL,MPI_MAX,MPI_COMM_WORLD,statinfo)
  print*,'te=',te
  Up=0

  !call MPI_ALLREDUCE(Un,Up,n,MPI_REAL,MPI_SUM,MPI_COMM_WORLD,statinfo)

  call Rename(Me,names)


  open (unit=me+1523,file=names)

  do i=beg,en
     call notloc (k,l,i,Nx)
     write(me+1523,*) k*dx,l*dy,Un(i)
  end do

  close (me+1523)


  call MPI_finalize(statinfo)













  deallocate (Un,Up,b)

end program main

