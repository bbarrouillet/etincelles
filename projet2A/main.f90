program main
  use fonctions
  use gradconj
  use MPI

  implicit none

real*8::td,te
  real::x,y,t,dx,dy,dt,D,Lx,Ly,eps,Isdx2,Isdy2
  integer::Nx,Ny,Nyloc,Np,statinfo,me,cas,beg,en
  integer::i,j,k,l,n,nl
integer,dimension(MPI_status_size)::status
  real,dimension(3)::A
  real,dimension(:), allocatable::Un,Up,b,testb

  open(unit=1000, file='parameter.dat', status='old')
  read(1000,*)Nx,Nyloc,dt,D,Lx,Ly
  close(1000)
 !  print*,Nx,Nyloc,dt,D,Lx,Ly
  dx=Lx/(Nx+1)
  Isdx2=1/dx/dx
  eps=0.000001
  cas=2
  nl=1000
  td=0
  te=0


!!!!!!!!!!!!!!!!!!!TZEST
Ny=100
dy=Ly/(Ny+1)
allocate (testb(Nx*Ny))




!!!!!!!!!!!!!!!!!!!!!!TESTS

    open(unit=2000,file="TEST_COND.dat") 

call notloc (k,l,9900,Nx)
print*,k,l
call notloc (k,l,800,Nx)

print*,k,l
print*,g(2,0.,0.),h(2,0.,0.),f(cas,0.990099013,0.980198026,0.,Lx,Ly),sin(0.990099013),cos(0.980198026)
	do i=1,Nx*Ny
        call notloc (k,l,i,Nx)

 testb(i)=dt*f(cas,k*dx,l*dy,t,Lx,Ly)

if (k==1) then
testb(i)=testb(i)+h(cas,k*dx,l*dy)*dt*D*Isdx2
elseif (k==Nx) then
testb(i)=testb(i)+h(cas,k*dx,l*dy)*dt*D*Isdx2
end if

if (l==1) then
testb(i)=testb(i)+g(cas,k*dx,l*dy)*dt*D*Isdy2
elseif (l==Ny) then
testb(i)=testb(i)+g(cas,k*dx,l*dy)*dt*D*Isdy2
end if  

write(2000,*) k*dx,l*dy,testb(i)

end do
  

 close(2000)


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!








  call MPI_INit(statinfo)
  td=MPI_WTIME()
  call MPI_COMM_RANK(MPI_COMM_WORLD,Me,statinfo)
  call MPI_COMM_SIZE(MPI_COMM_WORLD,Np,statinfo)
 t=0

  Ny=Nyloc*Np
  dy=Ly/(Ny+1)
  Isdy2=1/dy/dy
  call charge (Nx*Ny,Np,Me,beg,en)


 
!  print*,dy,Ly,Ny
  allocate (Un(Nx*Ny),Up(Nx*Ny),b(Nx*Ny))
  call rempAdiag (A,dx,dy,dt,D,Isdy2,Isdx2)
  
open(unit=13,file="matriceA.dat")

close(13) 
  n=Ny*Nx
print*,n
  Up=0
  Un=0
  




  


  do while (t<1)
 


     do i=beg,en
        call notloc (k,l,i,Nx)

 b(i)=Un(i)+dt*f(cas,k*dx,l*dy,t,Lx,Ly)

if (k==1) then
b(i)=b(i)+h(cas,0.,l*dy)*dt*D*Isdx2
elseif (k==Nx) then
b(i)=b(i)+h(cas,1.,l*dy)*dt*D*Isdx2
end if

if (l==1) then
b(i)=b(i)+g(cas,k*dx,0.)*dt*D*Isdy2
elseif (l==Ny) then
b(i)=b(i)+g(cas,k*dx,1.)*dt*D*Isdy2
end if     

end do



!On ettend Up pour qu'il soit compatible avec gradconj



if (Np/=1) then

if (me==0) then

call MPI_SEND(Up(en-Nx+1:en),Nx,MPI_REAL,1,Np+me+1,MPI_COMM_WORLD,statinfo)
call MPI_RECV(Up(en+1:en+Nx),Nx,MPI_REAL,1,1,MPI_COMM_WORLD,status,statinfo)


else if (me==Np-1) then 
call MPI_SEND(Up(beg:beg+Nx-1),Nx,MPI_REAL,Np-2,Np-1,MPI_COMM_WORLD,statinfo)
call MPI_RECV(Up(beg-Nx:beg-1),Nx,MPI_REAL,Np-2,Np-1+Np,MPI_COMM_WORLD,status,statinfo)


else
call MPI_SEND(Up(beg:beg+Nx-1),Nx,MPI_REAL,me-1,me,MPI_COMM_WORLD,statinfo)
call MPI_SEND(Up(en-Nx+1:en),Nx,MPI_REAL,me+1,Np+me+1,MPI_COMM_WORLD,statinfo)
call MPI_RECV(Up(beg-Nx:beg-1),Nx,MPI_REAL,me-1,Np+me,MPI_COMM_WORLD,status,statinfo)
call MPI_RECV(Up(en+1:en+Nx),Nx,MPI_REAL,me+1,me+1,MPI_COMM_WORLD,status,statinfo)
end if
end if
    call gradconjugue(A,b,Up,eps,nl,n,me,Np,beg,en,statinfo,Nx,Ny)



     Un=Up
     t=t+dt

print*, t
!print*,t




  end do

te=MPI_WTIME()-t
print*,'te=',te,'td=',td
Up=0
call MPI_ALLREDUCE(Un,Up,n,MPI_REAL,MPI_SUM,MPI_COMM_WORLD,statinfo)

if (me==0) then
  open (unit=14,file="solution.dat")
 do i=1,n
     call notloc (k,l,i,Nx)
     write(14,*) k*dx,l*dy,Up(i)
  end do
  close (14)
end if
  
  call MPI_finalize(statinfo)













  deallocate (Un,Up,b)

end program main



